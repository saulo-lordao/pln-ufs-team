#!/bin/sh

NLTK_DATA=./env/usr/local/share/nltk_data

echo "Criando ambiente virtual..."
if command -v virtualenv2 >/dev/null
then
  virtualenv2 env
else
  if test ! -s virtualenv.py
  then
    echo "Baixando virtualenv..."
    curl -L -o virtualenv.py https://raw.github.com/pypa/virtualenv/master/virtualenv.py
  fi

  if test ! -s get-pip.py
  then
    echo "Baixando pip..."
    curl -L -o get-pip.py https://bootstrap.pypa.io/get-pip.py
  fi

  python2 virtualenv.py --no-setuptools --no-pip --no-wheel env
fi

# Ativar ambiente virtual
. env/bin/activate

echo "Instalando pip..."
python2 get-pip.py >/dev/null 2>/dev/null

echo "Instalando dependências..."
pip2 install setuptools
pip2 install -r requirements.txt

mkdir -p $NLTK_DATA
python2 -m nltk.downloader -d $NLTK_DATA stopwords
python2 -m nltk.downloader -d $NLTK_DATA punkt

if ! command -v virtualenv2 >/dev/null
then
  tar --extract --file=WebCrawler/usr/script/srilm-1.7.1.tar.gz srilm-1.7.1/bin/i686-m64/ngram-count --strip=3
  mkdir ~/bin
  mv ngram-count ~/bin/
fi

echo "Favor iniciar nova sessão do terminal ou recarregar o arquivo de configuração do seu shell."
