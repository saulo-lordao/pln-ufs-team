# -*- coding: utf-8 -*-

"""
Created on 30/06/2015

@author: thiago
"""

import os
import subprocess



from random import randint
from django.utils import timezone
from DadosProcessamento import DadosProcessamento
from dominios import Dominios
from util import executar_comando, salvar_arquivo_json
from crawler_control_base import CrawlerFacadeBase
import oauth

"""
    Classe singleton para controle do Crawler
"""


class TwitterCrawlerFacade(CrawlerFacadeBase):

    def __init__(self, diretorio_trabalho, nome_usuario, recursos):
        super(TwitterCrawlerFacade, self).__init__(diretorio_trabalho, nome_usuario, recursos)
        self.dp.abrangencia = "twitter"
        self.tempo_espera = 10

    # Apaga conteúdo do diretório de trabalho e copia arquivos do crawler
    @property
    def _comando_configurar_diretorio_trabalho(self):
        return "%s/script/configurarDiretorioTrabalhoTwitterCrawler.sh '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos, self.recursos.nome_diretorio_usuario)

    @property
    def status_processamento(self):
        monitor_em_execucao = self.processamento_em_execucao
        if monitor_em_execucao and self._streaming_em_execucao:
            return "Baixando corpus"
        elif monitor_em_execucao:
            return "Processando corpus"
        elif self.corpus_compactado_gerado:
            return "Finalizado"
        return "Parado"

    @property
    def contagem_paginas(self):
        return self._ler__arquivo_contagem(self.recursos.nome_padrao_arquivo_contagem_paginas)

    @property
    def contagem_twittes(self):
        return self._ler__arquivo_contagem(self.recursos.nome_padrao_arquivo_contagem_twittes)

    def avaliar_dados_processamento(self, args):
        self.dp = DadosProcessamento.from_dict(args)
        self.dp.arquivo = self.recursos.nome_padrao_arquivo_dados_processamento

    def exportar_dados_acesso_twitter(self, dados):
        d = {
            "consumer_key": dados["consumer_key"],
             "consumer_secret": dados["consumer_secret"],
             "access_token": dados["access_token"],
             "access_token_secret": dados["access_token_secret"]
             }
        salvar_arquivo_json(self.recursos.nome_arquivo_dados_acesso_twitter, d)


    def iniciar_twitter_crawler(self, kargs):
        self.avaliar_dados_processamento(kargs)
        CrawlerFacadeBase.configurar_diretorio_trabalho(self)
        self.exportar_dados_acesso_twitter(kargs)

        self.dp.data_comeco = timezone.now()
        self.dp.salvar()
        conf = self.dp.as_dict()
        self.iniciar_twitter_streaming(conf)
        self.iniciar_twitter_monitor(conf)

    def iniciar_twitter_streaming(self, conf):
        comando = ["%s/streaming.py"%os.path.dirname(__file__)]
        comando.extend(["-usuario",  self.nome_usuario])
        comando.extend(["-palavras_chave", conf["palavras_chave"]])
        if conf["qtd_palavras"]:
            comando.extend(["-qtd_palavras", str(conf["qtd_palavras"])])

        if conf["eliminar_stopwords"]:
            comando.append("--stopwords")

        if conf["eliminar_pontuacao"]:
            comando.append("--pontuacao")

        if conf["limpar_caracteres"]:
            comando.append("--limpar_caracteres")
        return subprocess.Popen(comando).pid

    def iniciar_twitter_monitor(self, conf):
        comando = ["%s/monitor_twitter.py"%os.path.dirname(__file__)]
        comando.extend(["-usuario",  self.nome_usuario])
        comando.extend(["-wait", str(self.tempo_espera)])
        comando.extend(["-cmd_stop", self._comando_parar_streaming])
        comando.extend(["-cmd_running", self._comando_streaming_em_execucao])

        if conf["ativar_entidades"]:
            comando.append("--ativar_entidades")
            comando.extend(["-formato_saida_entidades", conf["formato_saida_entidades"]])

        if conf["ativar_ngram"]:
            if conf["gerar_vocabulario"]:
                comando.append("--gerar_vocabulario")
            comando.append("--ativar_ngram")
            comando.extend(["-numero_gram", str(conf["numero_gram"])])

        return subprocess.Popen(comando).pid

    def parar_crawler(self):
        return executar_comando(self._comando_parar_streaming)

    @property
    def _comando_parar_streaming(self):
        return "%s/script/stopTwitterStreaming.sh '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos, self.nome_usuario)

    @property
    def _streaming_em_execucao(self):
        saida = executar_comando(self._comando_streaming_em_execucao)
        return saida[0] not in ("\n", "")

    @property
    def _comando_streaming_em_execucao(self):
        return "%s/script/isTwitterStreamingRunning.sh '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos, self.nome_usuario)

    @property
    def _comando_processamento_em_execucao(self):
         return "ps awwux | grep monitor_twitter | grep %s | grep -v perp " \
                               "| grep -v grep | awk '{print $2}' | xargs" % self.nome_usuario

    @staticmethod
    def static_processamento_em_execucao(nome_usuario):
        saida = executar_comando("ps awwux | grep monitor_twitter | grep %s | grep -v perp " \
                               "| grep -v grep | awk '{print $2}' | xargs" % nome_usuario)
        return saida[0] not in ("\n","")

    def validar_credenciais_twitter(self, dados):
        oauth.verificar_credenciais(**dados)
