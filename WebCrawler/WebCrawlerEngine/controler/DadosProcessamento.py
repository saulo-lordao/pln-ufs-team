# -*- coding: utf-8 -*-
__author__ = 'thiago'

import dateutil.parser
from django.utils import timezone
import pytz
from util import ler_arquivo_json, salvar_arquivo_json

class DadosProcessamento():
    def __init__(self, arquivo = None):
        self.arquivo = arquivo
        self.ativar_ngram = False
        self.ativar_entidades = False
        self.gerar_vocabulario = False
        self.numero_gram = 0
        self.limpar_caracteres = False
        self.eliminiar_pontuacao = False
        self.eliminar_stopwords = False
        self.formato_saida_entidades = ""
        self.data_comeco = timezone.ZERO
        self.abrangencia = "web"
        self.palavras_chave = []
        self.qtd_palavras = 0

    @staticmethod
    def carregar_de_arquivo_json(nome_arquivo_json):
        return DadosProcessamento.from_dict(ler_arquivo_json(nome_arquivo_json))

    def salvar(self, nome_arquivo = None):
        arquivo = nome_arquivo or self.arquivo
        salvar_arquivo_json(arquivo, self.as_dict())

    @staticmethod
    def from_dict(dic):
        dados = DadosProcessamento()
        dados.ativar_ngram = bool(dic["ativar_ngram"])
        dados.ativar_entidades = bool(dic["ativar_entidades"])
        dados.numero_gram = int(dic["numero_gram"])
        dados.limpar_caracteres = bool(dic["limpar_caracteres"])
        dados.eliminiar_pontuacao = bool(dic["eliminar_pontuacao"])
        dados.eliminar_stopwords = bool(dic["eliminar_stopwords"])
        dados.qtd_palavras = dic["qtd_palavras"]
        dados.palavras_chave = dic["palavras_chave"]
        dados.formato_saida_entidades = dic["formato_saida_entidades"]
        dados.abrangencia = dic["abrangencia"]
        if "data_comeco" in dic:
               dados.data_comeco = dateutil.parser.parse(dic["data_comeco"]).astimezone(pytz.utc)
        dados.gerar_vocabulario = bool(dic["gerar_vocabulario"])
        return dados

    def as_dict(self):
        return {"ativar_ngram": self.ativar_ngram,
                "ativar_entidades": self.ativar_entidades,
                "gerar_vocabulario": self.gerar_vocabulario,
                "numero_gram":self.numero_gram,
                "limpar_caracteres": self.limpar_caracteres,
                "eliminar_pontuacao":self.eliminiar_pontuacao,
                "eliminar_stopwords": self.eliminar_stopwords,
                "formato_saida_entidades": self.formato_saida_entidades,
                "qtd_palavras": self.qtd_palavras,
                "palavras_chave": self.palavras_chave,
                "abrangencia": self.abrangencia,
                "data_comeco": self.data_comeco}
