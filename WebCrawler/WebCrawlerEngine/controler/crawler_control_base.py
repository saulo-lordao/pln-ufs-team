# -*- coding: utf-8 -*-
__author__ = 'thiago'

import abc
import os
from django.utils import timezone
from datetime import datetime
from DadosProcessamento import DadosProcessamento
from util import executar_comando


class CrawlerFacadeBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, diretorio_trabalho, nome_usuario, recursos):
        self.diretorio_trabalho = diretorio_trabalho
        self.nome_usuario = nome_usuario
        self.recursos = recursos
        self.db = None
        self.__carregar_dados_processamento(recursos.nome_padrao_arquivo_dados_processamento)

    def __carregar_dados_processamento(self, arquivo_configuracao):
        try:
            self.dp = DadosProcessamento.carregar_de_arquivo_json(arquivo_configuracao)
        except Exception as e:
            print ("Arquivo de dados de processamento não encontrado:" + str(e))
            self.dp = DadosProcessamento()
        self.dp.arquivo = arquivo_configuracao

    @abc.abstractproperty
    def _comando_configurar_diretorio_trabalho(self):
        raise NotImplementedError()

    def configurar_diretorio_trabalho(self):
        if not self.processamento_em_execucao:
            executar_comando(self._comando_configurar_diretorio_trabalho)

    @abc.abstractproperty
    def status_processamento(self):
        raise NotImplementedError()

    def _ler__arquivo_contagem(self, arquivo_contagem):
        try:
            with open(arquivo_contagem) as arq:
                return int(arq.read())
        except:
            return 0

    @property
    def contagem_palavras(self):
        return self._ler__arquivo_contagem(self.recursos.nome_padrao_arquivo_contagem_palavra)

    @property
    def data_geracao_corpora_compactado(self):
        return datetime.fromtimestamp(os.path.getmtime(self.recursos.nome_completo_arquivo_compactado))

    @property
    def corpus_compactado_gerado(self):
        return os.path.exists(self.recursos.nome_completo_arquivo_compactado)

    @property
    def tempo_processamento(self):
        if self.processamento_em_execucao:
            return (timezone.now() - self.dp.data_comeco).total_seconds()
        if self.corpus_compactado_gerado:
            return (self.data_geracao_corpora_compactado - timezone.make_naive(self.dp.data_comeco)).total_seconds()
        return 0

    @property
    def abrangencia(self):
        return self.dp.abrangencia

    @property
    def processamento_em_execucao(self):
        saida = executar_comando(self._comando_processamento_em_execucao)
        return saida[0] not in ("\n","")

    @abc.abstractproperty
    def _comando_processamento_em_execucao(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def parar_crawler(self):
        raise NotImplementedError()

    @property
    def abrangencia(self):
        return self.dp.abrangencia