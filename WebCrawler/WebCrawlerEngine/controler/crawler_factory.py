# -*- coding: utf-8 -*-



__author__ = 'thiago'

import os
from twitter_crawler_control import TwitterCrawlerFacade
from news_crawler_control import NewsCrawlerFacade
from DadosProcessamento import DadosProcessamento


class CrawlerFactory():

    def __init__(self, nome_usuario, recursos):
        self.nome_usuario = nome_usuario
        self.rec = recursos

    def get_crawler_em_execucao(self):
        if NewsCrawlerFacade.static_processamento_em_execucao(self.nome_usuario):
            return self.get_news_crawler()
        elif TwitterCrawlerFacade.static_processamento_em_execucao(self.nome_usuario):
            return self.get_twitter_crawler()

        try:
            dp = DadosProcessamento.carregar_de_arquivo_json(self.rec.nome_padrao_arquivo_dados_processamento)
            if dp.abrangencia == "web":
                return self.get_news_crawler()
            return self.get_twitter_crawler()
        except Exception as e:
            # criar por padrão um news crawler
            return self.get_news_crawler()


    def get_news_crawler(self):
        return NewsCrawlerFacade(self.rec.nome_diretorio_usuario, self.nome_usuario, self.rec)

    def get_twitter_crawler(self):
        return TwitterCrawlerFacade(self.rec.nome_diretorio_usuario, self.nome_usuario, self.rec)


