from __future__ import absolute_import, print_function

import tweepy

def verificar_credenciais(**kargs):
    auth = tweepy.OAuthHandler(kargs["consumer_key"], kargs["consumer_secret"])
    auth.secure = True
    auth.set_access_token(kargs["access_token"], kargs["access_token_secret"])
    api = tweepy.API(auth)
    api.verify_credentials()

