from django.conf.urls import url
from . import views

urlpatterns = [
    url('^$', views.index, name='index'),
    url('^iniciar/$', views.iniciar, name='iniciar'),
    url('^processamento/$', views.processamento, name='processamento'),
    url('^processamento/download/$', views.download_corpora, name='download_corpora'),
    url('^processamento/parar/$', views.parar_processamento, name='parar_processamento'),
]
